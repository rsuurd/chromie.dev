# Introduction

Chromie operates by implementing `ScheduledExecutorService` and persisting the task to a `ChromieRepository`, serializing the task using a `Serializer`.

A `ChromieDaemon` runs in the background querying the repository for tasks that should run and schedules those.
