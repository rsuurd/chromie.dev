# Getting started

## Dependency management

Chromie's available on [Maven Central](https://search.maven.org/search?q=g:dev.chromie).

For maven, use:

```xml
<dependency>
  <groupId>dev.chromie</groupId>
  <artifactId>chromie-core</artifactId>
  <version>1.0.0</version>
</dependency>
```

Or, for gradle:

```groovy
implementation 'dev.chromie:chromie-core:1.0.0'
```
