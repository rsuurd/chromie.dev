---
home: true
heroImage: /images/undraw_time_management_30iu.png
actionText: Get Started →
actionLink: /guide/
features:
  - title: Ease of use
    details: Drop-in replacement of your existing ScheduledExecutorService.
  - title: Extensible
    details: Implement your own serialization and storage strategies if needed.
  - title: Spring Boot Starter included
    details: Hit the ground running.
footer: Apache License 2.0 - © 2019 Rolf Suurd
---

# Be persistent

The standard `ScheduledExecutorService` in the Java SDK does a fine job at scheduling tasks, but they are lost when the JVM shuts down.

Chromie allows you to schedule `Runnable`s and `Callable`s using the `ScheduledExecutorService` like usual, but persists these tasks so they will be executed at the right time, regardless if your JVM has restarted a few times in between.

The most basic example is to create a scheduler and schedule something. For example, to take out the trash next week:

```java
  ScheduledExecutorService scheduler = Chromie.createScheduler();

  scheduler.schedule(new Reminder("Take out the trash"), 7, TimeUnit.DAYS);
```

The `Reminder` task is now persisted and will run next week.
