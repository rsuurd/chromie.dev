module.exports = {
 title: 'Chromie Scheduler',
 description: 'Persisted ScheduledFutures',
 dest: 'public',
 themeConfig: {
  nav: [
    { text: 'Home', link: '/' },
    { text: 'Guide', link: '/guide/' },
    { text: 'Gitlab', link: 'https://gitlab.com/rsuurd/chromie-scheduler' },
  ],
  sidebar: {
    '/guide/': [
      '',
      'getting-started'
    ],
  }
 }
}
